#ifndef _trie_
#define _trie_

#include "../list/list.h"
#include "../stack/stack.h"

#define CHILD_N 34
#define MAX_WORD_LENGTH 1000
//#define DEBUG

typedef struct TrieNode {
    char letter;
    int hit_count;
    struct TrieNode *parent;
    struct TrieNode *childs[CHILD_N];
} TrieNode;

TrieNode *trie_create(char letter, int initial_hit_count, TrieNode *parent);

TrieNode *trie_search(TrieNode *root, char *word);

TrieNode *trie_insert(TrieNode *root, char *word, int word_length);

List * trie_get_rank(TrieNode *root, int n);

void print_rank_list(List *list, int debug_mode);

#endif
