#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "trie.h"

int get_letter_value(char letter) {
    int val = letter - 97;
    if (val < 0) {
        val = letter - 65;
    }

    #ifdef DEBUG
    if (val > CHILD_N) {
        printf("DEBUG trie.get_letter_value: WARNING: letter value may be out of indexable range");
    }
    #endif

    return val;
}

TrieNode *trie_create(char letter, int initial_hit_count, TrieNode *parent) {
    TrieNode *node;

    node = malloc(sizeof(TrieNode));
    #ifdef DEBUG
    if (node == NULL) {
        printf("WARNING trie_create alloc FAILED\n");
    }
    #endif

    node->letter = letter;
    node->hit_count = initial_hit_count;
    node->parent = parent;

    return node;
}


TrieNode *trie_search(TrieNode *root, char *word) {
    TrieNode *current_node = root;
    int word_i = 0;
    int word_length = strlen(word);
    while (word_i < word_length) {
        char letter = word[word_i];
        int letter_val = get_letter_value(letter);
        TrieNode *child_node = current_node->childs[letter_val];

        if (child_node == NULL) {
            #ifdef DEBUG
            printf("DEBUG: trie.search finished NOT FOUND: %s\n", word);
            #endif
            return NULL;
        }

        current_node = child_node;
        ++word_i;
    }

    #ifdef DEBUG
    printf("DEBUG: trie.search: finished FOUND: %s - hit hit_count: %d\n",
           word,
           current_node->hit_count
    );
    #endif
    return current_node;
}

TrieNode *trie_insert(TrieNode *root, char *word, int word_length) {
    if (root == NULL) {
        root = trie_create(' ', 0, NULL);
    }

    TrieNode *current_node = root;
    int word_i = 0;
    while (word_i < word_length) {
        char letter = word[word_i];
        int letter_val = get_letter_value(letter);
        TrieNode *child_node = current_node->childs[letter_val];

        if (child_node == NULL) {
            current_node->childs[letter_val] = trie_create(letter, 0, current_node);
            child_node = current_node->childs[letter_val];
        }

        current_node = child_node;
        ++word_i;
    }

    current_node->hit_count++;

    #ifdef DEBUG
    printf("DEBUG: trie.insert: finished, hit hit_count for %s: %d\n",
           word,
           current_node->hit_count
    );
    #endif
    return root;
}

char *strrev(char *str) {
    if (!str || !*str)
        return str;

    int i = strlen(str) - 1, j = 0;

    char ch;
    while (i > j) {
        ch = str[i];
        str[i] = str[j];
        str[j] = ch;
        i--;
        j++;
    }
    return str;
}

char *get_node_word(TrieNode *node) {
    TrieNode *curr_node = node;
    char buffer[MAX_WORD_LENGTH];

    int i = 0;
    while (curr_node != NULL) {
        buffer[i] = curr_node->letter;
        ++i;
        curr_node = curr_node->parent;
    }

    buffer[i - 1] = '\0';
    strrev(buffer);
    return strdup(buffer);
}

void print_rank_list(List *list, int debug_mode) {
    ListNode *cl = list->first;
    if (debug_mode == 1) {
        printf("SIZE=%d: ", list->size);
    }
    while (cl != NULL) {
        TrieNode *tn = (TrieNode *) cl->payload;
        char *word = get_node_word(tn);
        if (debug_mode == 1) {
            printf("%s (%d), ", word, tn->hit_count);
        } else {
            printf("%s: %d\n", word, tn->hit_count);
        }
        free(word);
        cl = cl->next;
    }
    printf("\n");
}


List * trie_get_rank(TrieNode *root, int n) {
    List *nodes_rank_list = list_new();
    int cutout_count = 0;

    TrieNode *current_node = root;
    Stack *trie_visit_stack = stack_new();
    stack_push(trie_visit_stack, current_node);

    while (stack_get_size(trie_visit_stack)) {
        current_node = (TrieNode *) stack_pop(trie_visit_stack);
        //printf("stack: %p %d %d\n", current_node, current_node->hit_count, trie_visit_stack->size);
        if (current_node->hit_count > 0) {
            #ifdef DEBUG
            char *dbg_word = get_node_word(current_node);
            printf("DEBUG: trie.most_freq testing node with %s - hit_count: %d\n",
                   dbg_word,
                   current_node->hit_count
            );
            free(dbg_word);
            #endif
            int count = current_node->hit_count;
            if (count > cutout_count) {
                int update_cutout = 0;
                ListNode *current_list_node = nodes_rank_list->first;
                int inserted = 0;
                int list_count = 0;

                while (current_list_node != NULL) {
                    //printf("list: %p, %d, %d\n", current_list_node, list_count, nodes_rank_list->size);
                    TrieNode *cmp_node = (TrieNode *) current_list_node->payload;
                    if (cmp_node->hit_count < count) {
                        #ifdef DEBUG
                        printf("DEBUG: trie.most_freq SPLICING NODES RANK LIST\n");
                        #endif
                        list_insert_before(nodes_rank_list,
                                           current_list_node,
                                           current_node
                        );
                        inserted = 1;
                        break;
                    }
                    current_list_node = current_list_node->next;
                    list_count += 1;
                }
                if (nodes_rank_list->size > n) {
                    // will only reach after a splice that went over the limit (n)
                    #ifdef DEBUG
                    printf("DEBUG: trie.most_freq PRUNNING NODES RANK LIST\n");
                    #endif
                    list_remove(nodes_rank_list, nodes_rank_list->last);
                    update_cutout = 1;
                } else if (nodes_rank_list->size < n && inserted == 0) {
                    // inserts in the end of list...
                    #ifdef DEBUG
                    printf("DEBUG: trie.most_freq INSERTING AT END OF NODES RANK LIST \n");
                    #endif
                    list_insert(nodes_rank_list, current_node);
                    if (nodes_rank_list->size == n) {
                        update_cutout = 1;
                    }
                }
                if (update_cutout == 1) {
                    #ifdef DEBUG
                    printf("DEBUG: trie.most_freq updating cutout hit_count \n");
                    #endif
                    cutout_count = ((TrieNode *) nodes_rank_list->last->payload)->hit_count;
                }
            }
            #ifdef DEBUG
            printf("DEBUG: trie.most_freq nodes rank list so far:\n  ");
            print_rank_list(nodes_rank_list, 1);
            #endif
        }

        for (int i = 0; i < CHILD_N; ++i) {
            TrieNode *child_node = current_node->childs[i];
            if (child_node != NULL) {
                stack_push(trie_visit_stack, child_node);
            }
        }
    }

    #ifdef DEBUG
    printf("DEBUG: trie.most_freq finished, final result:\n  ");
    print_rank_list(nodes_rank_list, 1);
    #endif

    return nodes_rank_list;
}
