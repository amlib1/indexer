all: lib indexer trie-test

indexer: indexer.c trie.o stack.o list.o
	gcc -std=gnu99 -Wall -Wextra -o indexer indexer.c -Llib -ltrie -lstack -llist -lm

trie-test: trie-test.c trie.o stack.o list.o
	gcc -std=gnu99 -Wall -Wextra -o trie-test trie-test.c -Llib -ltrie -lstack -llist

lib: libtrie.a libstack.a liblist.a

libtrie.a: trie.o
	ar -rc libtrie.a trie.o
	ranlib libtrie.a
	mv libtrie.a lib
trie.o: trie/trie.c trie/trie.h
	gcc -std=gnu99 -Wall -Wextra -c trie/trie.c

libstack.a: stack.o
	ar -rc libstack.a stack.o
	ranlib libstack.a
	mv libstack.a lib
stack.o: stack/stack.c stack/stack.h
	gcc -std=gnu99 -Wall -Wextra -c stack/stack.c

liblist.a: list.o
	ar -rc liblist.a list.o
	ranlib liblist.a
	mv liblist.a lib
list.o: list/list.c list/list.h
	gcc -std=gnu99 -Wall -Wextra -c list/list.c

clean:
	rm -f *.o ./trie-test ./indexer "./lib/*.a"
