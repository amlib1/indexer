#ifndef _list_
#define _list_

typedef struct ListNode {
    void *payload;
    struct ListNode *next;
    struct ListNode *prev;
} ListNode;

typedef struct List {
    ListNode *first, *last;
    int size;
} List;

List *list_new();

int list_check_empty(List *list);

ListNode *list_insert_before(List *list, ListNode *target_node, void *payload);

ListNode *list_insert_after(List *list, ListNode *target_node, void *payload);

ListNode *list_insert(List *list, void *payload);

void *list_remove(List *list, ListNode *target_node);

void list_destroy(List *list, void (*payload_destroy)(void *));

ListNode *list_cycle(List *list, int (*comparator)(void *, void *), void *comparator_arg);

#endif