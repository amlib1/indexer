#include <stdlib.h>
#include "list.h"

List *list_new() {
    List *list = (List *) malloc(sizeof(List));
    list->first = NULL;
    list->last = NULL;
    list->size = 0;

    return list;
}

int list_check_empty(List *list) {
    if (list->first == NULL && list->last == NULL) {
        return 1;
    } else {
        return 0;
    }
}

// Create a new node with given payload and inserts it in the end of the list
ListNode *list_insert(List *list, void *payload) {
    ListNode *new_node = (ListNode *) malloc(sizeof(ListNode));
    new_node->next = NULL; // malloc won't guarantee that memory is initialized

    if (list->last == NULL || list->first == NULL) {
        list->last = new_node;
        list->first = new_node;
    } else {
        ListNode *last = list->last;
        last->next = new_node;
        new_node->prev = last;
        list->last = new_node;
    }

    list->size++;
    new_node->payload = payload;
    return new_node;
}

// Create a new node with given payload and inserts it before the given target_node
ListNode *list_insert_before(List *list, ListNode *target_node, void *payload) {
    ListNode *new_node = (ListNode *) malloc(sizeof(ListNode));
    new_node->payload = payload;

    if (target_node == list->first) {
        list->first = new_node;
    }

    new_node->prev = target_node->prev;
    target_node->prev = new_node;
    new_node->next = target_node;

    if (new_node->prev != NULL) {
        new_node->prev->next = new_node;
    }

    list->size++;
    return new_node;
}

// TODO TEST
// Create a new node with given payload and inserts it after the given target_node
ListNode *list_insert_after(List *list, ListNode *target_node, void *payload) {
    ListNode *new_node = (ListNode *) malloc(sizeof(ListNode));
    new_node->payload = payload;

    if (target_node == list->last) {
        list->last = new_node;
    }

    new_node->next = target_node->next;
    target_node->next = new_node;
    new_node->prev = target_node;

    if (new_node->next != NULL) {
        new_node->next->prev = new_node;
    }

    list->size++;
    return new_node;
}

// Removes given node from the list. Returns the payload,
//it is the caller responsibility to deal with it.
void *list_remove(List *list, ListNode *target_node) {
    if (list_check_empty(list) || target_node == NULL)
        return NULL;

    void *payload = target_node->payload;
    target_node->prev->next = target_node->next;

    if (target_node->next != NULL)
        target_node->next->prev = target_node->prev;
    else
        list->last = target_node->prev;
    free(target_node);

    list->size--;
    return payload;
}

// Destroys the whole list, including nodes and payloads. The payloads
//must be destroyed by a payload_destroy function provided by the caller
void list_destroy(List *list, void (*payload_destroy)(void *)) {
    if (payload_destroy != NULL)
        while (!list_check_empty(list))
            (*payload_destroy)(list_remove(list, list->last));

    free(list->first);
    free(list);
}

// Cycles through the list until the given comparator function returns 1
//or the end of the list is reached.
// Returns the node found or NULL if none
ListNode *list_cycle(List *list, int (*comparator)(void *, void *), void *comparator_arg) {
    ListNode *current_node = list->first->next;
    while (current_node != NULL) {
        if ((*comparator)(current_node->payload, comparator_arg))
            return current_node;
        current_node = current_node->next;
    }
    return NULL;
}
