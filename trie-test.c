#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#include "trie/trie.h"

int main() {
    TrieNode *root = trie_insert(NULL, "abacaxi", strlen("abacaxi"));

    root = trie_insert(root, "velociraptor", strlen("velociraptor"));
    root = trie_insert(root, "velociraptor", strlen("velociraptor"));
    root = trie_insert(root, "velociraptor", strlen("velociraptor"));
    root = trie_insert(root, "velociraptor", strlen("velociraptor"));
    root = trie_insert(root, "velociraptor", strlen("velociraptor"));
    root = trie_insert(root, "velociraptor", strlen("velociraptor"));

    root = trie_insert(root, "abacaxi", strlen("abacaxi"));
    root = trie_insert(root, "abacate", strlen("abacate"));
    root = trie_insert(root, "abacatelancia", strlen("abacatelancia"));
    root = trie_insert(root, "abacate", strlen("abacate"));
    root = trie_insert(root, "abacatelancia", strlen("abacatelancia"));
    root = trie_insert(root, "abacaxi", strlen("abacaxi"));

    root = trie_insert(root, "tobias", strlen("tobias"));
    root = trie_insert(root, "tobias", strlen("tobias"));
    root = trie_insert(root, "tobias", strlen("tobias"));
    root = trie_insert(root, "tobias", strlen("tobias"));
    root = trie_insert(root, "tobias", strlen("tobias"));
    root = trie_insert(root, "tobias", strlen("tobias"));
    root = trie_insert(root, "tobias", strlen("tobias"));
    root = trie_insert(root, "tobias", strlen("tobias"));
    
    root = trie_insert(root, "tocantins", strlen("tocantins"));
    root = trie_insert(root, "tocantins", strlen("tocantins"));
    root = trie_insert(root, "tocantins", strlen("tocantins"));
    root = trie_insert(root, "tocantins", strlen("tocantins"));

    trie_search(root, "abacaxi");
    trie_search(root, "abacaxixi");
    trie_search(root, "abacate");

    trie_get_rank(root, 3);
    trie_get_rank(root, 4);

    printf("done...\n");
}
