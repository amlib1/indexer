#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <math.h>

#include "list/list.h"
#include "trie/trie.h"

#define BUF_SIZE 1024 * 256
#define BUF_SIZE_EXTRA 1024

int is_ascii_alpha_char(unsigned char c) {
    if ((c > 65 && c < (65 + 34)) || (c > 97 && c < (97 + 34))) {
        return 1;
    }

    return 0;
}

char * get_word(char **stringp, int *word_len) {
    char *start = *stringp;
    while(**stringp != '\0') {
        if (!is_ascii_alpha_char(**stringp)) {
            *stringp = *stringp + 1;
            if (*stringp - start <= 1) {
                return NULL;
            }
            *word_len = *stringp - start - 1;
            return strndup(start, *stringp - start - 1);
        }
        *stringp = *stringp + 1;
    }

    *word_len = *stringp - start;
    return strndup(start, *stringp - start);
}

// TODO use double buffer to read words?
long int read_words_from_file(char *file_name, TrieNode *trie_root) {
    FILE *file = fopen(file_name, "r");
    if (file == NULL) {
        printf("ERROR: Could not open file: %s\n", file_name);
        exit(1);
    }

    long int words_read = 0;
    char *buf = malloc(BUF_SIZE + BUF_SIZE_EXTRA);
    int nread, word_len;
    while ((nread = fread(buf, 1, BUF_SIZE, file)) > 0) {
        char *bufp = buf + nread - 1;
        while (is_ascii_alpha_char(*bufp)) {
            ++bufp;
            *bufp = fgetc(file);
            if (bufp - buf >= BUF_SIZE_EXTRA * 2) {
                // maximum buffer size reached, word will be broken into two...
                break;
            }
        }
        *bufp = '\0';

        bufp = buf;
        char *w;

        int i = 0;
        while (*bufp != '\0') {
            w = get_word(&bufp, &word_len);
            if (w == NULL || word_len <= 2) {
                // printf("SKIPPING\n");
            } else {
                // printf("got word: %s\n", w);
                trie_insert(trie_root, w, word_len);
                words_read++;
            }
            i++;
            free(w);
        }

    }

    free(buf);
    return words_read;
}

int result_cmp(const void *a_in, const void *b_in) {
    double *a = (double *) a_in;
    double *b = (double *) b_in;

    return a - b;
}

struct Index {
    TrieNode *root;
    char *file_name;
    int long words_read;
    double term_frequency;
};

struct Result {
    char *file_name;
    double tf_idf;
};

int main(int argc, char **argv) {

    int mode = 0, scan_count = 0;
    int n = 0;
    char *word = NULL;
    List *terms = NULL;
    char *file_name = NULL;
    while ( argc > 1 && **(argv+1) == '-' ) {
        ++argv; --argc;
        if ( strcmp(*argv, "--freq") == 0 ) {
            mode = 1;

            --argc; scan_count = sscanf(*++argv, "%d", &n);
            if (scan_count <= 0) {
                printf("ERROR: Missing or invalid N parameter\n");
                exit(1);
            }

            --argc; file_name = *++argv;
            if (file_name == NULL) {
                printf("ERROR: Missing FILE parameter\n");
                exit(1);
            }

            continue;
        }

        if ( strcmp(*argv, "--freq-word") == 0 ) {
            mode = 2;

            --argc; word = *++argv;
            if (word == NULL) {
                printf("ERROR: Missing WORD parameter\n");
                exit(1);
            }

            --argc; file_name = *++argv;
            if (file_name == NULL) {
                printf("ERROR: Missing FILE parameter\n");
                exit(1);
            }

            continue;
        }

        if ( strcmp(*argv, "--search") == 0 ) {
            mode = 3;

            --argc; char *str_terms = *++argv;
            if (str_terms == NULL) {
                printf("ERROR: Missing TERM parameter\n");
                exit(1);
            }

            terms = list_new();
            char *pstr_terms = str_terms;
            while(1) {
                pstr_terms = index(str_terms, ' ');
                if (pstr_terms == NULL) {
                    list_insert(terms, strdup(str_terms));
                    break;
                }

                *pstr_terms = '\0';
                list_insert(terms, strdup(str_terms));
                pstr_terms++;
                str_terms = pstr_terms;
            }

            if (argc <= 1) {
                printf("ERROR: Search mode needs at least one file!\n");
                exit(1);
            }

            continue;
        }

        printf("ERROR: Unrecognized argument: %s, aborting!\n", *argv);
        exit(1);
    }
    ++argv; --argc;

    if (mode == 1) { // freq
        printf("Rading and generating index...\n");
        TrieNode *indexer_root = trie_create(' ', 0, NULL);
        read_words_from_file(file_name, indexer_root);
        printf("Ranking hit counts...\n");
        List *rank = trie_get_rank(indexer_root, n);
        printf("Result:\n");
        print_rank_list(rank, 0);
    } else if (mode == 2) { // freq-word
        TrieNode *indexer_root = trie_create(' ', 0, NULL);
        printf("Rading and generating index...\n");
        read_words_from_file(file_name, indexer_root);
        printf("Retrieving word...\n");
        TrieNode *node = trie_search(indexer_root, word);
        int count;
        if (node == NULL) {
            count = 0;
        } else {
            count = node->hit_count;
        }

        printf("%s: %d\n", word, count);
    } else if (mode == 3) { // search
        List *indexes = list_new();
        while(argc > 0) {
            char *file_name = *argv;
            printf("Reading and generating index for file: %s...\n", file_name);
            TrieNode *root = trie_create(' ', 0, NULL);
            long int words_read = read_words_from_file(file_name, root);
            
            struct Index *index = (struct Index *) calloc(1, sizeof(struct Index));
            index->root = root;
            index->file_name = file_name;
            index->words_read = words_read;

            list_insert(indexes, index);
            ++argv;
            argc--;
        }

        ListNode *curr_term = terms->first;
        while (curr_term != NULL) {
            printf("Results for term: %s\n", (char *) curr_term->payload);
            ListNode *curr_index = indexes->first;
            struct Result results[indexes->size];

            int term_in_doc_n = 0;
            while(curr_index != NULL) {
                struct Index *index = (struct Index *) curr_index->payload;

                TrieNode *trie_node = trie_search(index->root, (char *) curr_term->payload);
                if (trie_node != NULL) {
                    term_in_doc_n++;
                }

                curr_index = curr_index->next;
            }

            double idf = log10((double) indexes->size / (double) term_in_doc_n);
            if (term_in_doc_n == 0.0) { // avoids division by zero
                idf = 0.0;
            }

            curr_index = indexes->first;
            int i = 0;
            while(curr_index != NULL) {
                struct Index *index = (struct Index *) curr_index->payload;

                TrieNode *trie_node = trie_search(index->root, (char *) curr_term->payload);
                int term_frequency = 0;
                if (trie_node != NULL) {
                    term_frequency = trie_node->hit_count;
                }

                double tf = (double) term_frequency / (double) index->words_read;
                double tf_idf = tf * idf;
                //if (idf == 0.0) { // when all terms existis in all files idf is 0.0, ignore it?
                //    tf_idf = tf;
                //}
                results[i].file_name = index->file_name;
                results[i].tf_idf = tf_idf;

                curr_index = curr_index->next;
                i++;
            }

            qsort(results, indexes->size, sizeof(struct Result), &result_cmp);

            for (int j = 0; j < indexes->size; ++j) {
                struct Result res = results[j];
                printf("  %s: %.15f\n", res.file_name, res.tf_idf);
            }

            curr_term = curr_term->next;
        }
    }

}
