#include <stdlib.h>
#include "stack.h"

Stack *stack_new() {
    Stack *stack = malloc(sizeof(Stack));
    stack->size_allocated = ALOC_STEP;
    stack->size = 0;
    stack->payload = (void **) malloc(sizeof(void *) * ALOC_STEP);
    return stack;
}

// Pushes a payload to the stack
void stack_push(Stack *stack, void *payload) {
    // Re-allocates stack if there is no more room left
    if (++stack->size >= stack->size_allocated) {
        stack->size_allocated = stack->size_allocated + ALOC_STEP;
        stack->payload = (void **) realloc(stack->payload, sizeof(void *) * stack->size_allocated);
    }
    *(stack->payload + stack->size - 1) = payload;
}

// Returns the payload at the top of the stack
void *stack_pop(Stack *stack) {
    if (stack->size) {
        // Downsizes the stack if it has shrunk down enough
        if (--stack->size < stack->size_allocated - ALOC_STEP) {
            stack->size_allocated = stack->size_allocated - ALOC_STEP;
            stack->payload = (void **) realloc(stack->payload, sizeof(void *) * stack->size_allocated);
        }
        return *(stack->payload + stack->size); //stack->size-1+1
    }
    return NULL;
}

int stack_get_size(Stack *stack) {
    return stack->size;
}

// Destroys the whole stack by popping each payload and destroying it
//with the given payload_destroy function
void stack_destroy(Stack *stack, void (*payload_destroy)(void *)) {
    if (payload_destroy != NULL)
        while (stack->size > 0)
            (*payload_destroy)(stack_pop(stack));
    free(stack);
}

// Returns a pointer to the top of the stack
void *stack_get_top(Stack *stack) {
    return *(stack->payload + stack->size - 1);
}
