#ifndef _stack_
#define _stack_

#define ALOC_STEP 16

typedef struct Stack {
    void **payload;
    int size_allocated;
    int size;
} Stack;

Stack *stack_new();

void stack_push(Stack *stack, void *payload);

void *stack_pop(Stack *stack);

int stack_get_size(Stack *stack);

void stack_destroy(Stack *stack, void (*payload_destroy)(void *));

void *stack_get_top(Stack *stack);
#endif
